provider "aws" {
  region = var.aws_region
}

# Creates IAM role for SFTP server
resource "aws_iam_role" "aws_transfer_service" {
  name = "aws_transfer_service_role"

  assume_role_policy = file("${path.module}/template/assumeRolePolicy.json")
}

# Gives SFTP server role above access to s3 bucket
resource "aws_iam_role_policy" "aws_transfer_service" {
  name = "aws_transfer_service_policy"
  role = aws_iam_role.aws_transfer_service.id

  policy = templatefile("${path.module}/template/sftpRolePolicy.tftpl", {
    bucket_arn = aws_s3_bucket.b.arn
  })
}

# Create s3 bucket with private ACL and blocking public access
resource "aws_s3_bucket" "b" {
  bucket = var.bucket_name
}
resource "aws_s3_bucket_public_access_block" "public_access_block" {
  bucket = aws_s3_bucket.b.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# If var.auto_delete_days is over 0 it will create this lifecycle that auto 
# deletes files after # of days you spicfy in the variable since the upload date
resource "aws_s3_bucket_lifecycle_configuration" "lifecycle" {
  count  = var.auto_delete_days > 0 ? 1 : 0
  bucket = aws_s3_bucket.b.id
  rule {
    id     = "auto_delete"
    status = "Enabled"
    expiration {
      days = var.auto_delete_days
    }
  }
}

# Create sftp server
resource "aws_transfer_server" "sftp_server" {
  identity_provider_type = "SERVICE_MANAGED"

  protocols    = ["SFTP"]
  logging_role = aws_iam_role.aws_transfer_service.arn
}
# Create sftp user 
resource "aws_transfer_user" "sftp_user" {
  server_id      = aws_transfer_server.sftp_server.id
  user_name      = var.sftp_username
  role           = aws_iam_role.aws_transfer_service.arn
  home_directory = "/${aws_s3_bucket.b.bucket}"
}

# Outputs SFTP server host URL
output "sftp_server_endpoint" {
  description = "The endpoint of the SFTP server"
  value       = aws_transfer_server.sftp_server.endpoint
}
