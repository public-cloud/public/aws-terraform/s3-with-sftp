variable "aws_region" {
  type        = string
  description = "aws region to use"
  default     = "us-east-2"
}

variable "bucket_name" {
  type        = string
  description = "Name of s3 bucket"
  default     = "edx-dcs-integration"
}

variable "sftp_username" {
  type        = string
  description = "user name of sftp user"
  default     = "test-edx"
}
# If this is 0 it will not auto delete files. If it is >=1 it will auto delete files after that number of days from being uploaded. 
variable "auto_delete_days" {
  type        = number
  description = "# of days once a file is auto deleted"
  default     = 0
}
